////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

public class Matrix:CustomStringConvertible{

// MARK : Private Properties
    private var data:Array<Double> = Array<Double>()
    private var rows:Int = 0
    private var columns:Int = 0
    
// MARK : Public Properties
    /*
       Creates matrix with given rows and colums as dimension
     */
    init( data:Array<Double> , rows:Int, columns:Int){
        self.data = data
        self.rows = rows
        self.columns = columns
    }

    public var dimension:(rows:Int,columns:Int){
        get{ return (rows,columns) }
    }

    /*
        A matrix with one row or one column
     */
    public var isVector:Bool{
        get{
            if (rows == 1 && columns == 0) || (rows == 0 && columns == 1){ return true }
            else{ return false }
        }
    }
    
    /*
        A matrix with  count of rows equal to count of colums
     */
    public var isSquare:Bool{

        get{ return (rows == columns) }
    }
    

    /*
        A scalar matrix is a special kind of diagonal matrix.
        It is a diagonal matrix with equal-valued elements along the diagonal.
     */
    public var isScalar:Bool{
        
        get{
            if !isSquare { return false}
            let prev = self[1,1]
            for rw in 1...dimension.rows{
                for cl in 1...dimension.columns{
                    if cl != rw && self[rw,cl] != 0 {
                        return false
                    }else if(cl == rw  && prev != self[rw,cl] ){
                        return false
                    }
                }
            }
            
            return true
        }
    }
    
    /*
        A square matrix in which all the elements of the principal diagonal are ones
        and all other elements are zeros.
        The effect of multiplying a given matrix by an identity matrix is to leave the given matrix unchanged.
     */
    public var isIdentity:Bool{
        get{
            if !isSquare { return false}
            let prev = 1.0
            for rw in 1...dimension.rows{
                for cl in 1...dimension.columns{
                    if cl != rw && self[rw,cl] != 0 {
                        return false
                    }else if(cl == rw  && prev != self[rw,cl] ){
                        return false
                    }
                }
            }
            
            return true
        }
    }
    
    /*
        The transpose of a matrix is a new matrix whose rows are the columns of the original.
        (This makes the columns of the new matrix the rows of the original).
     */
    public var transpose:Matrix{
        get{
            var elements:Array<Double> = Array<Double>()
            for cl in 1...self.dimension.columns{
                for rw in 1...self.dimension.rows{ elements.append(self[rw,cl]) }
            }
            
            return Matrix(data: elements, rows: self.dimension.columns, columns: self.dimension.rows)
        }
    }
    
    /*
        This property can ebused for print the Matrix
     */
    public var description: String{
        
        get{
            var desc:String = ""
            for rw in 1...self.rows{
                for cl in 1...self.columns{
                    let valStr = String(format:"\0%.1f\t\t",self[rw,cl])
                    desc += "\(valStr)"
                }
                desc += "\n"
            }
            
            return desc
        }
    }
    
    
// MARK : Public Functions
    
    /*
        For a square matrix A, the inverse is written A-1. When A is multiplied by A-1 the result is the identity matrix I. Non-square matrices do not have inverses
     */
    public func inverse()->Matrix{
        
        let dt = self.deteminant()
        let transpose = self.transpose
        let adj = transpose.adjugateMatrix()
        
        return   adj * (1 / dt)
    }

/*
     The determinant of a matrix is a special number that can be calculated from a square matrix.
     For details : https://www.mathsisfun.com/algebra/matrix-determinant.html
     
     This func returns the determinant from the self
 */
    public func deteminant()->Double{ return Matrix.deteminant(matrix: self) }
    
    /*
     The determinant of a matrix is a special number that can be calculated from a square matrix.
     For details : https://www.mathsisfun.com/algebra/matrix-determinant.html
     
     This func returns the determinant from the given matrix
     */
    public static func deteminant(matrix:Matrix)->Double{
        
        if matrix.dimension.rows == 2 && matrix.dimension.columns == 2{
            return (matrix[1,1] * matrix[2,2]) - ( matrix[1,2] * matrix[2,1])
        }
        var sign:Double = 1
        var result:Double = 0
        for cl in 1...matrix.dimension.columns{
            let multiplier = matrix[1,cl]
            let matrixMinor:Matrix = Matrix.matrixOfMinors(matrix: matrix,row :1 , column: cl)
            result += (multiplier * deteminant(matrix: matrixMinor)) * sign
            sign *= -1
        }
        
        return result
    }

/*
    Subscript is used for accessing elements in the matrix based on the row and column
*/
    public subscript(row: Int, col: Int) -> Double  {
        
        get {
            if row <= rows && row > 0 && col <= columns && col > 0{
                let idx = ((row - 1)  * columns) + (col - 1)
                return data[idx]
            }else{
                print("Invalid row or column")
                return -1
            }
        }
        set {
            if row <= rows && row > 0 && col <= columns && col > 0{
                let idx = ((row - 1)  * columns) + (col - 1)
                self.data[idx] = newValue
            }else{ print("Invalid row or column") }
        }
    }
    
    
// MARK : Private functions
    
/*
      Adjugate matrix. In linear algebra, the adjugate, classical adjoint, or adjunct of a square matrix is the transpose of its cofactor matrix. The adjugate has sometimes been called the "adjoint", but today the "adjoint" of a matrix normally refers to its corresponding adjoint operator, which is its conjugate transpose.
 */
    private func adjugateMatrix()->Matrix{
        
        var elements:Array<Double> = Array<Double>()
        var sign:Double = 1
        for lrw in 1...self.dimension.rows{
            for lcl in 1...self.dimension.columns{
                let matrixMinor:Matrix = Matrix.matrixOfMinors(matrix: self, row:lrw, column: lcl)
                let dt = matrixMinor.deteminant() * sign
                elements.append(dt)
                sign *= -1
            }
        }
        
        return Matrix(data: elements, rows: self.dimension.rows, columns: self.dimension.columns)
    }
    
/*
     A "minor" is the determinant of the square matrix formed by deleting one row and one column from some larger square matrix. Since there are lots of rows and columns in the original matrix, you can make lots of minors from it. These minors are labelled according to the row and column you deleted.
 */
    private static func matrixOfMinors(matrix:Matrix , row : Int, column:Int)->Matrix{
        
        var elements:Array<Double> = Array<Double>()
        for rw in 1...matrix.dimension.rows{
            for cl in 1...matrix.dimension.columns{
                if cl == column || row == rw { continue }
                elements.append(matrix[rw,cl])
            }
        }
        
        return Matrix(data: elements,rows: matrix.dimension.rows - 1 , columns: matrix.dimension.columns - 1)
    }
    

// MARK : Operators
    
    /* Sum of two matrices */
    public static func +(left:Matrix , right:Matrix)->Matrix{
        
        if left.dimension != right.dimension{ return left }
        var elements:Array<Double> = Array<Double>()
        for rw in 1...left.dimension.rows{
            for cl in 1...left.dimension.columns{
                let element:Double = left[rw,cl] + right[rw , cl]
                elements.append(element)
            }
        }
     
        return Matrix(data: elements, rows: left.dimension.rows, columns: left.dimension.columns)
    }
    /* Multiply two matrices */
    public static func *(left:Matrix , right:Matrix)->Matrix{
        
        if left.dimension.columns != right.dimension.rows{ return left }
        var elements:Array<Double> = Array<Double>()
        for lrw in 1...left.dimension.rows{
            for rcl in 1...right.dimension.columns{
                var sigleElelment = 0.0
                for lcl in 1...left.dimension.columns{
                    sigleElelment += left[lrw,lcl] * right[lcl,rcl]
                }
                elements.append(sigleElelment)
            }
        }
        
        return Matrix(data: elements, rows: left.dimension.rows, columns: left.dimension.rows)
    }
    
    /* Multiply a matrix  with a number*/
    public static func *(left:Matrix , right:Double)->Matrix{
        
        var elements:Array<Double> = Array<Double>()
        for lrw in 1...left.dimension.rows{
            for lcl in 1...left.dimension.columns{
                elements.append(left[lrw,lcl] * right)
            }
        }
        
        return Matrix(data: elements, rows: left.dimension.rows, columns: left.dimension.rows)
    }
    
    /* Check equality of two matrices */
    public static func ==(left:Matrix,right:Matrix)->Bool{
        
        for rw in 1...left.dimension.rows{
            for cl in 1...left.dimension.columns{
                if left[rw,cl] != right[rw,cl]{ return false }
            }
        }
    
        return true
    }
}







