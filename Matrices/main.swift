////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//
import Foundation



let matrix1 :Matrix = Matrix(data: [6,1,1,4,-2,5,2,8,7], rows: 3, columns: 3)

print("\(matrix1)")
print("\(matrix1.transpose)")

let matrix2 :Matrix = Matrix(data: [2,1,-4,3,-4,1,2,-2,6], rows: 3, columns: 3)
let matrix3 :Matrix = Matrix(data: [1,5,3,2,4,7,4,6,2], rows: 3, columns: 3)
let matrix4 :Matrix = Matrix(data: [1,-2,4,-6,3,-3,5,2,-4,2,3,1,3,1,4,6], rows: 4, columns: 4)

print("\(matrix1.deteminant())")
print("\(matrix2.deteminant())")
print("\(matrix3.deteminant())")
print("\(matrix4.deteminant())")


let matrix4Inverse = matrix4.inverse()

print("\(matrix4Inverse)")

let identity = matrix4Inverse * matrix4

print("\(identity)")

let matrix5 :Matrix = Matrix(data: [5,0,0,0, 0,5,0,0 ,0,0,5,0,0,0,0,5], rows: 4, columns: 4)

print("\(matrix5)")
print("\(matrix5.isScalar)")

